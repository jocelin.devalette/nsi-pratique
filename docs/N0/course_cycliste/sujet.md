---
author: Pierre Marquestaut
title: La course cycliste
tags:
  - 0-simple
---

# La course cycliste

Une course cycliste se dispute. 

![course](https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/V%C3%A9locourse.jpg/220px-V%C3%A9locourse.jpg) <br/>
*source : Wikipedia*

Chaque cycliste est identifié par un numéro de dossard : `"Nadia-01"`, `"Franck-64"`

Les cyclistes se doublent les uns les autres durant la course.

Il peut arriver qu'un cycliste crève, et se fasse doubler par tous les autres coureurs et se retrouve à la dernière place.

Les coureurs sont identifiés par leur prénom et leurs positions sont stockées de façon ordonnée dans un tableau.

Exemple :
```python
course = ["Nadia-01", "Franck-64", "Thomas-31", "Elizabeth-22", "Laure-66"]
```
Dans cet exemple, Nadia est la première et Laure la dernière.

* **Créer** la fonction `premier` qui prend en paramètre le tableau qui stocke la course et qui renvoie le coureur en première position de la course.

```pycon
>>> premier(course)
"Nadia-01"
```

* **Créer** la fonction `dernier` qui prend en paramètre le tableau qui stocke la course et qui renvoie le coureur en dernière position de la course.

```pycon
>>> dernier(course)
"Laure-66"
```

* **Créer** la fonction `position` qui prend en paramètre le tableau qui stocke la course et le numéro de dossard et qui renvoie sa position dans la course.

```pycon
>>> position("Thomas-31")
2
```

* **Créer** la fonction `double` qui prend en paramètre le tableau qui stocke la course et le dossard d'un coureur et modifie le tableau avec le coureur qui est passé devant le coureur précédent.

```pycon
>>> double(course, "Franck-64")
>>> premier(course)
"Franck-64"
```

* **Créer** la fonction `crevaison` qui prend en paramètre le tableau qui stocke la course et le dossard d'un coureur qui a crevé et modifie le tableau. On partira du principe que tous les coureurs derrière le malchanceux coureur le doublent jusqu'à ce que le coureur soit le dernier.
On utilisera **obligatoirement** les fonctions `double` et `dernier`.


```pycon
>>> crevaison(course, "Thomas-31")
>>> dernier(course)
"Thomas-31"
```



{{ IDE('exo') }}
