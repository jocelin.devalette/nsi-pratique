def convertir_secondes(duree):
    heures, minutes, secondes = duree
    return heures * 3600 + minutes * 60 + secondes

def convertir_min_sec(duree):
    return duree // 60, duree % 60

def convertir_h_m_s(duree):
    heures = duree // 3600
    secondes_restantes = duree % 3600
    return heures, secondes_restantes // 60, secondes_restantes % 60
