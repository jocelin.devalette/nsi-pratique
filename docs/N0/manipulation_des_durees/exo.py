def convertir_secondes():
    ...

def convertir_min_sec():
    ...

def convertir_h_m_s():
    ...

# Tests
assert convertir_secondes((1, 25, 50)) == 5150
assert convertir_min_sec(125) == (2, 5)
assert convertir_h_m_s(125) == (0, 2, 5)

