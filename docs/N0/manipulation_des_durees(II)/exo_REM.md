{{ IDE('exo_corr') }}

On aurait pu également passer trois paramètres à la fonctions, le dernier (celui des `heures`) ayant une **valeur par défaut**.

```python
def convertir_secondes(secondes, minutes, heures = 0):
    return (heures * 60 + minutes) * 60 + secondes
```
Ainsi, on peut ensuite appeler la fonction avec trois arguments :

```pycon
>>> convertir_secondes(50, 25, 1)
5150
```
ou avec seulement deux arguments :

```pycon
>>> convertir_secondes(20, 2)
140
```
Dans ce dernier cas, le paramètre `heures` prend la valeur par défaut `0`.
