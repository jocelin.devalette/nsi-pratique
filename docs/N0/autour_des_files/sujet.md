---
author: Pierre Marquestaut
title: Autour des files
tags:
  - 0-simple
  - 7-files
---

## Autour des files

Les files sont des listes particulières dont les éléments ne peuvent être ajoutés qu'en tête, et retirés qu'à partir de la queue.


![files](file.svg)

Ce fonctionnement est appelé **FIFO** : First In, First Out.

C'est le principe des files d'attente : les personnes entre dans une file et en ressortent dans le même ordre dans lequel elles sont rentrées.

* **enfile** : action d'ajouter un élément en tête de la file
* **défile** : action de retirer un élément de la queue de la file

En Python, les files peuvent être créées par le type `list`.
Ce type dispose de méthodes pour ajouter ou supprimer des éléments.

* La méthode `append` permet d'ajouter un élément en queue de liste.

```python
>>> nouvelle_liste = [1, 2, 3]
>>> nouvelle_liste.append(4)
>>> nouvelle_liste
[1, 2, 3, 4]
```

* La méthode `pop` retire un élément de la liste et le renvoie. Sans paramètre, elle retire par défaut l'élément en queue de la liste.
```python
>>> nouvelle_liste = [1, 2, 3, 4]
>>> nouvelle_liste.pop()
4
>>> nouvelle_liste
[1, 2, 3]
>>> nouvelle_liste.pop(0)
1
>>> nouvelle_liste
[2, 3]
```
## Au travail
**Compléter** le code ci-dessous :

{{ IDE('exo') }}
