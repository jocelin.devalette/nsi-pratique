def calcul_puissance(tension, intensite):
    return tension * intensite

def calcul_energie(puissance, temps):
    return puissance * temps

def cout_utilisation(energie, prix_base):
    return round(energie * prix_base, 2)

def cout_consommation(intensite, temps):
    puissance = calcul_puissance(230, intensite)
    energie = calcul_energie(puissance, temps)
    prix = 0.1582
    return cout_utilisation(energie / 1000, prix)
