assert calcul_puissance(230, 20) == 4600
assert calcul_energie(4600, 1.5) == 6900
assert cout_utilisation(6.9, 0.1582) == 1.09
assert cout_consommation(20, 1.5) == 1.09

assert calcul_puissance(230, 0) == 0
assert calcul_energie(4600, 0) == 0
assert cout_utilisation(6.9, 1) == 6.9
assert cout_utilisation(0, 1) == 0
assert cout_consommation(20, 0) == 0
