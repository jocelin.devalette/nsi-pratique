# Tests
assert poids_boite([]) == 0
assert poids_boite([7, 5, 3, 1]) == 16
assert rangement([7, 5, 3, 1], 8) == [[7, 1], [5, 3]]
assert rangement([7, 5, 3, 1], 16) == [[7, 5, 3, 1]]

# Tests supplémentaires
assert poids_boite([-1, 1]) == 0
assert poids_boite([-1, -1]) == -2
assert rangement([10, 10, 10], 10) == [[10], [10], [10]]
assert rangement([0, 0, 0], 15) == [[0, 0, 0]]
assert rangement([9, 4, 3, 3], 10) == [[9], [4, 3, 3]]
assert rangement([9, 9, 1, 1], 10) == [[9, 1], [9, 1]]
assert rangement([9, 9, 9, 1], 10) == [[9, 1], [9], [9]]
