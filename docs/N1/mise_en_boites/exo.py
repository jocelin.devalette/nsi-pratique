def poids_boite(boite):
    ...


def rangement(poids_objets, poids_max):
    ...


# Tests
assert poids_boite([]) == 0
assert poids_boite([7, 5, 3, 1]) == 16
assert rangement([7, 5, 3, 1], 8) == [[7, 1], [5, 3]]
assert rangement([7, 5, 3, 1], 16) == [[7, 5, 3, 1]]
