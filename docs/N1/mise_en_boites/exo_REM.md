Il s'agit du problème classique du *bin-packing*. On applique ici la méthode de la première position : on place chaque objet dans la première boîte pouvant l'accueillir.

# Commentaires

{{ IDE('exo_corr') }}

La fonction `poids_boite` ne présente pas difficultés : on crée une variable `total` initialisée à `0` puis on parcourt les objets et on additionne chaque poids à ce `total`.

Dans la fonction `rangement` :

* on crée la liste `boites` comme le suggère l'énoncé,
* on parcourt l'ensemble des objets :
    * pour chacun d'entre eux, on se demande s'il tient dans la dernière boîte. Si ce n'est pas le cas, on en ajoute une nouvelle,
    * on ajoute l'objet à la dernière boîte
* on la liste `boites`