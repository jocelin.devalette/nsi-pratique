---
author: Nicolas Revéret
title: Livraisons à Manhattan
tags:
    - boucle
status: relecture
---

# Livraisons à Manhattan

> On n'utilisera pas `min`, `sort` ou `sorted` dans cet exercice.


Un livreur new-yorkais organise sa tournée. Le quartier dans lequel il évolue est un quadrillage :
toutes les rues suivent l'axe Nord ↔ Sud ou l'axe Est ↔ Ouest.

Les adresses de ses livraisons sont donc des couples de coordonnées, `#!py (2, 3)` par exemple. La première coordonnée est l'*abscisse* du point, la seconde son *ordonnée*.

Ces adresses sont données dans une liste python. Par exemple :

```python
adresses = [(2, 0), (0, 1), (3, 3), (2, 3)]
```

Ce qui correspond à la carte ci-dessous.

![carte.PNG](carte.PNG){ width="50%" }

Afin de déterminer son itinéraire, il décide de toujours livrer en priorité l'adresse la plus "proche" de sa position actuelle. Se déplaçant le long des rues, il mesure la distance $AB$ entre deux points de coordonnées $(x_A\,; y_A)$ et $(x_B\,; y_B)$ en faisant :

$$AB = \text{abs}(x_B-x_A)+\text{abs}(y_B-y_A)$$

où $\text{abs}(x)$ est la valeur absolue du nombre $x$ : $\text{abs}(3-5)=\text{abs}(5-3)=2$.

!!! tip "Coup de pouce"
    Avec Python, `#!py abs(x)` renvoie la valeur absolue de `x`

Vous devez écrire deux fonctions :

* une fonction `distance(xA, yA, xB, yB)` calculant la distance entre deux points dont on fournit les coordonnées
* une fonction `prochaine_livraison(adresses, pos_x, pos_y)` prenant en argument la liste des adresses à livrer et la position actuelle du livreur (`pos_x` et `pos_y`) et renvoyant les coordonnées du prochain point à livrer

Si deux points sont à la même distance de la position actuelle du livreur on renverra les coordonnées du premier point rencontré dans la liste.

!!! example "Exemples"

    ```pycon
    >>> distance(0, 1, 2, 3)
    4
    >>> adresses = [(2, 0), (0, 1), (3, 3), (2, 3)]
    >>> prochaine_livraison(adresses, 0, 0)
    (0, 1)
    >>> adresses = [(2, 0), (3, 3), (2, 3)]
    >>> prochaine_livraison(adresses, 0, 1)
    (2, 0)
    >>> adresses = [(3, 3), (2, 3)]
    >>> prochaine_livraison(adresses, 2, 0)
    (2, 3)
    >>> adresses = [(3, 3)]
    >>> prochaine_livraison(adresses, 2, 3)
    (3, 3)
    >>> adresses = [(1, 0), (0, 1)]
    >>> prochaine_livraison(adresses, 0, 0)
    (1, 0)
    ```

{{ IDE('exo') }}
