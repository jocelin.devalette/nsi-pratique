# Tests
assert repetitions(5, [2, 5, 3, 5, 6, 9, 5]) == 3
assert repetitions("A", ["B", "A", "B", "A", "R"]) == 2
assert repetitions(12, [1, 7, 21, 36, 44]) == 0
assert repetitions(12, []) == 0

# Tests supplémentaires
import random

for test in range(1, 6):
    element = random.randint(-10, 10)
    valeurs = [random.randint(-10, 10) for _ in range(randint(800, 1000))]
    assert repetitions(element, valeurs) == valeurs.count(element), f"Erreur dans le test caché n°{test}"
