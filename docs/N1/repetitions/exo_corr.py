def repetitions(cible, valeurs):
    nb = 0
    for elt in valeurs:
        if elt == cible:
            nb = nb + 1
    return nb


# Tests
assert repetitions(5, [2, 5, 3, 5, 6, 9, 5]) == 3
assert repetitions("A", ["B", "A", "B", "A", "R"]) == 2
assert repetitions(12, [1, 7, 21, 36, 44]) == 0
assert repetitions(12, []) == 0
