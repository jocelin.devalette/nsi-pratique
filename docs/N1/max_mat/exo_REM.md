Pas forcément simple.

Variantes : 

- on renvoie le plus petit numéro de colonne contenant le maximum de 1

```python
def max_col(tab):
    maxc = 0
    for j in range(len(tab[0])):
        somj = 0
        for i in range(len(tab)):
            somj += tab[i][j]
        if somj > maxc:
            maxc = somj
    return maxc
```

- même exo mais avec la recherche des numéros de ligne

```python
def max_row(tab):
    maxr = 0
    inds_maxr = []
    for i in range(len(tab)):
        somi = 0
        for j in range(len(tab[0])):
            somi += tab[i][j]
        if somi > maxc:
            inds_maxr = [i]
            maxr = somi
        elif somi > 0 and somi == maxr:
            inds_maxc.append(i)
    return inds_maxr
```


- on demande plutôt de rajouter une ligne avec le nombre de 1 par colonnes


```python
def bord_row(tab):
    new_tab = tab[:]
	new_tab.append([0 for _ in range(len(new_tab[0]))])
    for j in range(len(tab[0])):
        somj = 0
        for i in range(len(tab)):
            somj += tab[i][j]
        new_tab[len(tab)][j] = somj
    return new_tab
```
