# Commentaires

{{ IDE('exo_corr') }}

La fonction `noircir` effectue les calculs sur tous les caractères de `texte` : il ainsi plus immédiat de directement parcourir les caractères au lieu des indices dans la boucle `for`.

Il est aussi possible d'écrire cette fonction en utilisant une approche plus « fonctionnelle » :

```python
def noircir(texte, noir):
    return "".join([noir if c.isalpha() else c for c in texte])
```
