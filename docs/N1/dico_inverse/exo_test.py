# tests
dico = {"a": 5, "b": 7}
assert trier_valeurs(inverser(dico)) == {5: ["a"], 7: ["b"]}

dico = {"a": 5, "b": 7, "c": 5}
assert trier_valeurs(inverser(dico)) == {5: ["a", "c"], 7: ["b"]}

dico = {
    "Paris": "Tour Eiffel",
    "Rome": "Colisée",
    "Berlin": "Reichtag",
    "Londres": "Big Ben",
}
assert trier_valeurs(inverser(dico)) == {
    "Tour Eiffel": ["Paris"],
    "Colisée": ["Rome"],
    "Reichtag": ["Berlin"],
    "Big Ben": ["Londres"],
}

dico = {"Paris": "P", "Lyon": "L", "Nantes": "N", "Lille": "L"}
assert trier_valeurs(inverser(dico)) == {
    "P": ["Paris"],
    "L": ["Lille", "Lyon"],
    "N": ["Nantes"],
}

# autres tests

assert inverser({}) == {}
assert inverser({"a": "a"}) == {"a": ["a"]}
assert trier_valeurs(inverser({1: 2, 2: 2, 3: 2})) == {2: [1, 2, 3]}
