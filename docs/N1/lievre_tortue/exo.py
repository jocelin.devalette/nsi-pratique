class Cellule():
    def __init__(self):
        self.info = None
        self.suivante = None

def contient_boucle(tete: Cellule) -> bool:
    ...  # À compléter


# Tests

# liste test_1 : 18 cellules avec boucle
test_1 = Cellule()
test_1.info = 0

case = test_1
for i in range(1, 18):
    suite = Cellule()
    suite.info = i
    if i == 13:
        debut_boucle = suite
    case.suivante = suite
    case = suite
case.suivante = debut_boucle 

# liste test_2 : 8 cellules, sans boucle
test_2 = Cellule()
test_2.info = 0

case = test_2
for i in range(1, 8):
    case.suivante = Cellule()
    case = case.suivante
    case.info = i

# liste test_3 : liste vide
test_3 = Cellule()

# liste test_4 : une boucle globale
test_4 = Cellule()
test_4.info = 0

case = test_4
for i in range(1, 8):
    case.suivante = Cellule()
    case = case.suivante
    case.info = i

case.suivante = test_4

# liste test5 : 2 cellules en boucle
test_5 = Cellule()
test_5.info = 1
test_5.suivante = Cellule()
test_5.suivante.info = 2
test_5.suivante.suivante = test_5


assert contient_boucle(test_1) == True
assert contient_boucle(test_2) == False
assert contient_boucle(test_3) == False
assert contient_boucle(test_4) == True
assert contient_boucle(test_5) == True
assert contient_boucle(test_6) == False

