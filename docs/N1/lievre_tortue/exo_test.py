# à refaire ; facile, presque une copie des premiers tests
# mettre des tests avec des cellules qui ont la même info


# liste test_3
test_3 = Cellule()

# liste test_4
prec = test_4 = Cellule()
test_4.info = 0
for i in range(1, 100):
    prec.suivante = Cellule()
    prec.suivante.info = i
    prec = prec.suivante

prec.suivante = test_4

# liste test_5 : 2 cellules
test_5 = Cellule()
test_5.info = 1
test_5.suivante = Cellule()
test_5.suivante.info = 2
test_5.suivante.suivante = test_5

# liste test_6 : 7 cellules sans boucle
prec = test_6 = Cellule()
test_6.info = 0
for i in range(1, 7):
    prec.suivante = Cellule()
    prec.suivante.info = i
    prec = prec.suivante


# liste test_7 : 7 cellules même info sans boucle
prec = test_7 = Cellule()
test_7.info = 7
for i in range(1, 7):
    prec.suivante = Cellule()
    prec.suivante.info = "7"
    prec = prec.suivante


assert contient_boucle(test_3) == False
assert contient_boucle(test_4) == True
assert contient_boucle(test_5) == True
assert contient_boucle(test_6) == False
assert contient_boucle(test_7) == False
