# Commentaires

{{ IDE('exo_corr') }}

La fonction `caviarder` n'effectue des transformations que sur une certaine plage d'indices. La boucle `for` doit donc se faire sur ceux-ci.

Il est aussi possible d'écrire cette fonction en utilisant une approche plus « fonctionnelle » :

```python
def caviarder(texte, debut, fin):
    return "".join(["#" if debut <= i <= fin and c.isalpha() else c for i, c in enumerate(texte)])
```
