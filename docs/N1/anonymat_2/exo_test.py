from random import randint, choice, sample

# Tests
assert caviarder("L'espion était J. Bond", 15, 21) == "L'espion était #. ####"
assert caviarder("Paul est un espion", 100, 200) == "Paul est un espion"


# Tests supplémentaires
ALPHABET = "".join(chr(i) for i in range(ord("A"), ord("A") + 26))
ALPHABET += "".join(chr(i) for i in range(ord("a"), ord("a") + 26))
PONCTUATION = " ,;:!?.()[]'"

for _ in range(10):
    nb_lettres = randint(1, 20)
    nb_ponctuation = randint(1, 20)
    texte = "".join([choice(ALPHABET) for _ in range(nb_lettres)])
    texte += "".join([choice(PONCTUATION) for _ in range(nb_ponctuation)])
    texte = "".join(sample(texte, len(texte)))
    debut = randint(0, len(texte) - 1)
    fin = randint(debut, len(texte) - 1)
    assert caviarder(texte, debut, fin) == "".join(
        ["#" if debut <= i <= fin and c.isalpha() else c for i, c in enumerate(texte)]
    )
