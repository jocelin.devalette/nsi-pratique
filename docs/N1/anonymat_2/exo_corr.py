def caviarder(texte, debut, fin):
    resultat = ""
    for i in range(len(texte)):
        caractere = texte[i]
        if debut <= i <= fin and caractere.isalpha():
            resultat += "#"
        else:
            resultat += caractere

    return resultat


# Tests
assert caviarder("L'espion était J. Bond", 15, 21) == "L'espion était #. ####"
assert caviarder("Paul est un espion", 100, 200) == "Paul est un espion"
