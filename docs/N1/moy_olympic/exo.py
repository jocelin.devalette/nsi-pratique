def moyenne_bourree(notes):
    ...


notes1 = [1.0, 2.0, 4.0, 5.0]
notes2 = [1.0, 1.0, 1.0, 1.0, 1.0]


assert round(moyenne_bourree(notes1),1) == 3.0
assert round(moyenne_bourree(notes2),1) == 1.0
