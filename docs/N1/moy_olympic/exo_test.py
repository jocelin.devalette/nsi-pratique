notes1 = [1.0, 2.0, 4.0, 5.0]
notes2 = [1.0, 1.0, 1.0, 1.0, 1.0]


assert round(moyenne_bourree(notes1),1) == 3.0
assert round(moyenne_bourree(notes2),1) == 1.0

notes1 = [2, -1, 2, 10, 2]
notes2 = [1, 1, 1, 1, 1, 1]
assert round(moyenne_bourree(notes1),1) == 2.0
assert round(moyenne_bourree(notes2),1) == 1.0

try :
    notes3 = [1]
    moyenne_bourree(notes3)
except ZeroDivisionError:
    assert False, "Un nombre insuffisant de juges doit générer une erreur !"
except AssertionError:
    pass
else:
    assert False, "Un nombre insuffisant de juges doit générer une erreur !"
    
    
try :
    notes3 = [1,3]
    moyenne_bourree(notes3)
except ZeroDivisionError:
    assert False, "Un nombre insuffisant de juges doit générer une erreur !"
except AssertionError:
    pass
else:
    assert False, "Un nombre insuffisant de juges doit générer une erreur !"
    
