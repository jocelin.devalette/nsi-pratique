{{ IDE('exo_corr') }}

Pour résoudre cet exercice, il fallait rechercher les valeurs minimum et maximum, puis calculer la moyenne.

Il est possible d'utiliser trois boucle, mais la solution proposée avec une seule boucle permet de minimiser le nombres d'instructions.

En revanche, dans les deux cas, la complexité de l'algorithme reste linaire.
