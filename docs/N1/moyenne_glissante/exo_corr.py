def somme(serie):
    s = 0
    for element in serie:
        s = s + element
    return s


def moyenne(serie):
    return somme(serie)/len(serie)


def moyenne_glissante(serie):
    if len(serie) < 7:
        return []
    resultat = []
    for indice in range(6, len(serie)):
        sous_serie = [serie[indice - 7 + i] for i in range(7)]
        resultat = resultat + [moyenne(sous_serie)]
    return resultat
