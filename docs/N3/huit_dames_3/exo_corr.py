def est_valide(disposition, n):
    # Vérifications des lignes
    nb_dames_placees = len(disposition)
    lignes_occupees = [False] * n
    for ligne in disposition:
        if lignes_occupees[ligne]:
            return False
        else:
            lignes_occupees[ligne] = True

    # Vérification des diagonales
    for j1 in range(nb_dames_placees - 1):
        for j2 in range(j1 + 1, nb_dames_placees):
            if abs(disposition[j2] - disposition[j1]) == (j2 - j1):
                return False
    return True


def cherche_valides(disposition, n):
    nb_dames_placees = len(disposition)

    if nb_dames_placees == n:  # on a placé toutes les dames
        dispositions_valides.append(disposition.copy())
    else:  # il reste des dames à placer
        for ligne in range(n):
            disposition.append(ligne)
            if est_valide(disposition, n):
                cherche_valides(disposition, n)
            disposition.pop()


# Tests
# Echiquier de dimension n = 3
dispositions_valides = []
cherche_valides([], 3)
assert dispositions_valides == []

# Echiquier de dimension n = 4
dispositions_valides = []
cherche_valides([], 4)
assert sorted(dispositions_valides) == [[1, 3, 0, 2], [2, 0, 3, 1]]
