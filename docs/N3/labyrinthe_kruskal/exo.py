#--- HDR ---#
import random


class Cellule:
    def __init__(self, zone):
        self.est = True
        self.sud = True
        self.zone = zone

    def __str__(self) -> str:
        if self.est and self.sud:
            return "_|"
        elif self.est:
            return " |"
        elif self.sud:
            return "__"
        else:
            return "  "

    def __repr__(self) -> str:
        return self.__str__()


def base_labyrinthe(hauteur, largeur):
    lab = []
    for i in range(hauteur):
        ligne = [Cellule(i * largeur + j) for j in range(largeur)]
        lab.append(ligne)
    return lab


def dessin_console(labyrinthe):
    lignes = ["\n " + "__" * (LARGEUR - 1) + "_"]
    for i in range(HAUTEUR):
        ligne = ["|"] + [str(labyrinthe[i][j]) for j in range(LARGEUR)]
        lignes.append("".join(ligne))
    dessin = "\n".join(lignes) + "\n"
    return dessin


def murs_aleatoires(hauteur, largeur, graine=1):
    murs = []
    for i in range(hauteur - 1):
        for j in range(largeur - 1):
            murs.append((i, j, "est"))
            murs.append((i, j, "sud"))
    for i in range(hauteur - 1):
        murs.append((i, largeur - 1, "nord"))
    for j in range(largeur - 1):
        murs.append((hauteur - 1, j, "est"))
    random.seed(graine)
    random.shuffle(murs)
    return murs


#--- HDR ---#
def labyrinthe(hauteur, largeur):
    # Initialisation
    lab = base_labyrinthe(hauteur, largeur)
    murs = murs_aleatoires(hauteur, largeur)

    # Traitement
    while ... != 0:
        i1, j1, orientation = murs.pop()
        ...

    return lab


# Test
HAUTEUR, LARGEUR = 4, 10
lab = labyrinthe(HAUTEUR, LARGEUR)
assert (
    dessin_console(lab)
    == """
 ___________________
|    _|  ___| |__ | |
|_|  _|__ |__  _____|
|     |____ |     | |
|_|_|_________|_|___|
"""
)
