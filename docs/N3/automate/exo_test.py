# tests
# Expression régulière cherchée : (ab*c)*aa|b
regles = {(0, "a"): 1, (0, "b"): 2, (1, "a"): 2, (1, "b"): 1, (1, "c"): 0}
debut = 0
fin = 2

correspond("b", regles, debut, fin) == True
correspond("abbcaa", regles, debut, fin) == True
correspond("abbcb", regles, debut, fin) == True
correspond("abbc", regles, debut, fin) == False
correspond("abbbd", regles, debut, fin) == False
correspond("ba", regles, debut, fin) == False

# tests supplémentaires
# Expression régulière cherchée : ba+to+
regles = {(0, "b"): 1, (1, "a"): 2, (2, "a"): 2, (2, "t"): 3, (3, "o"): 4, (4, "o"): 4}
debut = 0
fin = 2

correspond("bato", regles, debut, fin) == True
correspond("baato", regles, debut, fin) == True
correspond("batoo", regles, debut, fin) == True
correspond("baaaatoo", regles, debut, fin) == True
correspond("aatoo", regles, debut, fin) == False
correspond("baaat", regles, debut, fin) == False
correspond("btooo", regles, debut, fin) == False
