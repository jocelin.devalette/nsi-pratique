# Tests
# un + un == deux
assert addition(UN, UN) == DEUX
# trois + cinq == huit
assert addition(TROIS, DEUX) == CINQ
# zéro * cinq == zéro
assert multiplication(ZERO, CINQ) == ZERO
# deux * trois == six
assert multiplication(DEUX, TROIS) == SIX
# cinq - trois  == deux
assert soustraction(CINQ, TROIS) == DEUX
# trois - cinq -> Opération impossible
try:
    soustraction(DEUX, CINQ)
except ValueError:
    pass

# Tests supplémentaires
ZERO = tuple()
nombres = [ZERO]
for k in range(1, 1_000):
    nombres.append(successeur(nombres[-1]))
from random import randrange

for test in range(10):
    i_a = randrange(0, 30)
    i_b = randrange(0, 30)
    i_a, i_b = tuple(sorted([i_a, i_b]))
    a = nombres[i_a]
    b = nombres[i_b]
    # Test de la somme
    attendu = nombres[i_a + i_b]
    assert addition(a, b) == attendu, f"Erreur sur l'addition de '{i_a}' et '{i_b}'"
    # Test de la différence valide
    attendu = nombres[i_b - i_a]
    assert (
        soustraction(b, a) == attendu
    ), f"Erreur sur la soustraction de '{i_b}' et '{i_a}'"
    # Test de la différence invalide
    try:
        soustraction(a, b)
    except ValueError:
        pass
    except:
        raise AssertionError(f"Erreur sur la soustraction de '{i_a}' et '{i_b}'. Il faut lever une erreur de valeur.")
    # Test du produit
    attendu = nombres[i_a * i_b]
    assert (
        multiplication(a, b) == attendu
    ), f"Erreur sur le produit de '{i_b}' et '{i_a}'"
