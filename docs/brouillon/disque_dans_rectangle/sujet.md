---
author: Franck Chambon
title: Disque dans rectangle
tags:
  - 4-maths
  - 2-tuple
---
# Nombre de tirs dans une zone rectangulaire

Dans cet exercice, on considère une zone rectangulaire dont les côtés sont parallèles aux axes des coordonnées.

- Le coin en bas à gauche est `p_A` de coordonnées `(x_min, y_min)`
- Le coin en haut à droite est `p_B` de coordonnées `(x_max, y_max)`

Un rectangle est donné par les deux points `(p_A, p_B)` dans un tuple.

Un disque est donné par un tuple `(x, y, r)` où

- `x` est l'abscisse du centre du disque
- `y` est l'ordonnée du centre du disque
- `r` est le rayon du disque

Écrire deux fonctions telles que :

- `disque_est_dans_rectangle(x, y, r, x_min, y_min, x_max, y_max)` détermine (en renvoyant un booléen) si le disque de centre `(x, y)` et de rayon `r` est entièrement inclus dans le rectangle `(p_A, p_B)`, sachant que `p_A = (x_min, y_min)` et `p_B = (x_max, y_max)` ?
- `nb_disques_dans_rectangle(rectangle, disques)` renvoie le nombre de disques qui sont entièrement inclus dans le rectangle.
    - `disques` est une liste qui contient des disques.
    - `rectangle` est un rectangle définit par un tuple `(p_A, p_B)` de points.
    - Les points `p_A` et `p_B` sont des tuples de deux coordonnées.

!!! example "Exemples"

    TODO figure

    ```pycon
    >>> disque_est_dans_rectangle(
    ... x=30, y=53, r=1,
    ... x_min=10, y_min=50, x_max=80, y_max=60)
    True
    >>> disque_est_dans_rectangle(
    ... x=30, y=53, r=10,
    ... x_min=10, y_min=50, x_max=80, y_max=60)
    False
    >>> disque_est_dans_rectangle(
    ... x=0, y=53, r=1,
    ... x_min=10, y_min=50, x_max=80, y_max=60)
    False
    ```

    ```pycon
    >>> nb_disques_dans_rectangle(((10, 50), (80, 60)), [])
    0
    >>> nb_disques_dans_rectangle(((10, 50), (80, 60)),
    ... [(30, 53, 1), (30, 53, 10), (0, 53, 1)])
    1
    ```

{{ IDE('exo') }}
