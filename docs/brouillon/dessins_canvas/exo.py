#--- HDR ---#
from js import document as doc

canvas = doc.getElementById("canvas")
largeur = canvas.parentElement.clientWidth
hauteur = largeur
canvas.setAttribute("width", largeur)  # doc.body.clientWidth)
canvas.setAttribute("height", hauteur)
ctx = canvas.getContext("2d")
ctx.fillStyle = "#eeeeee"
ctx.fillRect(0, 0, largeur, hauteur)


def rectangle_plein(x, y, largeur, hauteur, couleur, context=None):
    """
    Dessine un rectangle plein dont le coin supérieur gauche est en (x, y)
    On fournit de plus la largeur et la hauteur (entiers positifs)
    ainsi que la couleur de remplissage sous forme d'une chaîne en hexa "#FF0000"
    """
    if context is None:
        context = ctx
    ctx.fillStyle = couleur
    ctx.fillRect(x, y, largeur, hauteur)

def rectangle_vide(x, y, largeur, hauteur, couleur, context=None):
    """
    Dessine un rectangle vide dont le coin supérieur gauche est en (x, y)
    On fournit de plus la largeur et la hauteur (entiers positifs)
    ainsi que la couleur du bord sous forme d'une chaîne en hexa "#FF0000"
    """
    if context is None:
        context = ctx
    ctx.strokeStyle = couleur
    ctx.strokeRect(x, y, largeur, hauteur)
#--- HDR ---#


rectangle_plein(20, 20, 100, 100, "#FF0000")
rectangle_vide(30, 30, 50, 70, "#00FF00")
