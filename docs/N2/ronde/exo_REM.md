# Commentaires

{{ IDE('exo_corr') }}

Comme on est sûr que la liste `booleens` contient au moins une valeur `True`, on peut construire la fonction autour de `while True:` tout en ayant la certitude de sortir de la boucle.

Dans la boucle :

* on incrément `indice` et l'on vérifie qu'il ne dépasse pas la `taille` de la liste (si oui, on lui donne la valeur `0`)
* on teste si `booleens[indice]` est égal à `True`. Si c'est le cas on renvoie la valeur de `indice`

Dans la fonction `gagnant`, on commence par créer la liste de booléens, tous égaux à `True` (tous les joueurs sont encore en jeu).

On désigne le joueur dont c'est le tour avec `indice_joueur` (à `0` au début pour le joueur 1).

On lance ensuite la partie qui continue jusqu'à ce que le nombre de joueurs soit égal à `1`.

A chaque étape :

* on cherche l'indice du prochain joueur encore en jeu
* on  l'élimine
* on passe au joueur suivant en effectuant une nouvelle recherche

A la fin de la fonction, on renvoie `indice_joueur+1` afin de tenir compte du décalage entre les indices dans la liste et les numéros des joueurs.