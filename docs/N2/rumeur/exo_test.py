# Tests supplémentaires
graphe = {'Camille':  ['Romane', 'Marion', 'Paul'],
          'Romane':   ['Nicolas', 'Antoine', 'Paul'],
          'Marion':   ['Camille', 'Romane'],
          'Paul':     ['Antoine', 'Romane'],
          'Antoine':  ['Nicolas'],
          'Nicolas':  ['Camille', 'Antoine'],
          'Stéphane': ['Antoine']}

assert distance(graphe, 'Camille', 'Camille') == 0
assert distance(graphe, 'Camille', 'Romane') == 1
assert distance(graphe, 'Camille', 'Marion') == 1
assert distance(graphe, 'Camille', 'Paul') == 1
assert distance(graphe, 'Camille', 'Antoine') == 2
assert distance(graphe, 'Camille', 'Nicolas') == 2
assert distance(graphe, 'Camille', 'Stéphane') is None
assert distance(graphe, 'Romane', 'Camille') == 2
assert distance(graphe, 'Romane', 'Romane') == 0
assert distance(graphe, 'Romane', 'Marion') == 3
assert distance(graphe, 'Romane', 'Paul') == 1
assert distance(graphe, 'Romane', 'Antoine') == 1
assert distance(graphe, 'Romane', 'Nicolas') == 1
assert distance(graphe, 'Romane', 'Stéphane') is None
assert distance(graphe, 'Marion', 'Camille') == 1
assert distance(graphe, 'Marion', 'Romane') == 1
assert distance(graphe, 'Marion', 'Marion') == 0
assert distance(graphe, 'Marion', 'Paul') == 2
assert distance(graphe, 'Marion', 'Antoine') == 2
assert distance(graphe, 'Marion', 'Nicolas') == 2
assert distance(graphe, 'Marion', 'Stéphane') is None
assert distance(graphe, 'Paul', 'Camille') == 3
assert distance(graphe, 'Paul', 'Romane') == 1
assert distance(graphe, 'Paul', 'Marion') == 4
assert distance(graphe, 'Paul', 'Paul') == 0
assert distance(graphe, 'Paul', 'Antoine') == 1
assert distance(graphe, 'Paul', 'Nicolas') == 2
assert distance(graphe, 'Paul', 'Stéphane') is None
assert distance(graphe, 'Antoine', 'Camille') == 2
assert distance(graphe, 'Antoine', 'Romane') == 3
assert distance(graphe, 'Antoine', 'Marion') == 3
assert distance(graphe, 'Antoine', 'Paul') == 3
assert distance(graphe, 'Antoine', 'Antoine') == 0
assert distance(graphe, 'Antoine', 'Nicolas') == 1
assert distance(graphe, 'Antoine', 'Stéphane') is None
assert distance(graphe, 'Nicolas', 'Camille') == 1
assert distance(graphe, 'Nicolas', 'Romane') == 2
assert distance(graphe, 'Nicolas', 'Marion') == 2
assert distance(graphe, 'Nicolas', 'Paul') == 2
assert distance(graphe, 'Nicolas', 'Antoine') == 1
assert distance(graphe, 'Nicolas', 'Nicolas') == 0
assert distance(graphe, 'Nicolas', 'Stéphane') is None
assert distance(graphe, 'Stéphane', 'Camille') == 3
assert distance(graphe, 'Stéphane', 'Romane') == 4
assert distance(graphe, 'Stéphane', 'Marion') == 4
assert distance(graphe, 'Stéphane', 'Paul') == 4
assert distance(graphe, 'Stéphane', 'Antoine') == 1
assert distance(graphe, 'Stéphane', 'Nicolas') == 2
assert distance(graphe, 'Stéphane', 'Stéphane') == 0

graphe = {'A': ['B'],
          'B': ['C'],
          'C': []}

assert distance(graphe, 'A',  'B') == 1
assert distance(graphe, 'A',  'C') == 2
assert distance(graphe, 'B', 'C') == 1
assert distance(graphe, 'C', 'B') is None
