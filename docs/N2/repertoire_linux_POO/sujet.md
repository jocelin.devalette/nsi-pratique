---
author: Pierre Marquestaut
title: Copie d'un tableau
tags:
  - 2-POO
  - 2-récursivité
---

# Répertoire Linux

Un répertoire peut contenir des fichiers (images, textes...) mais aussi d'autres répertoires qui peuvent également contenir des fichiers ou d'autres répertoires et ainsi de suite...

On présente ci-dessous un extrait de l'arborescence des répertoire sous Linux :
![arborescence](arborescence.svg)

Le répertoire not `/`est le répertoire racine aussi appelé `root`.

```python
>>> root = Fichier("root", True)
>>> etc = root.creer_repertoire("etc")
>>> home = root.creer_repertoire("home")
>>> home.creer_fichier("log.txt")
>>> repertoireTP = home.creer_repertoire("TP")
>>> repertoireTP.creer_fichier("TP1")
>>> repertoireTP.creer_fichier("TP2")
>>> root.affiche()
root
-- etc
-- home
---- log.txt
---- TP
------ TP1.odt
------ TP2.odt
```

```python
>>> home.vider()
Le répertoire TP n'est pas vide
 root
-- etc
-- home
---- TP
------ TP1.odt
------ TP2.odt
```

```python
>>> root.vider_rec()
>>> root.affiche()
root
-- etc
-- home
```





{{ IDE('exo') }}
