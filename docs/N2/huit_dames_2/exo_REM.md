## Commentaires

{{ IDE('exo_corr') }}

La vérification de la présence d'une seule dame par ligne utilise le tableau de booléens décrit dans l'énoncé.

Concernant les diagonales on utilise le test `if abs(disposition[j2] - disposition[j1]) == (j2 - j1):` où `j1` et `j2` sont les indices des colonnes étudiées (`j2` > `j1`). Supposons que les dames des colonnes `3` et `7` soient sur la même diagonale orientée vers le nord-est. L'écart `j2 - j1` entre les indices des colonnes doit alors être égal à l'écart *en valeur absolue** entre les indices des lignes concernés.