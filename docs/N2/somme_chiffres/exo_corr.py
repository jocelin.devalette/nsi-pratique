def somme_chiffres(effectifs_chiffres):
    # assert len(effectifs_chiffres) == 10, Le tableau doit être de taille 10
    resultat = 0
    for i in range(10):
        resultat = resultat + effectifs_chiffres[i] * i
    return resultat

def decomposition(nombre):
    effectifs_chiffres = [0] * 10
    while nombre != 0:
        nombre, unite = nombre // 10, nombre % 10
        effectifs_chiffres[unite] = effectifs_chiffres[unite] + 1
    return effectifs_chiffres

