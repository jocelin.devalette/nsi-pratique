def dichotomie(tableau, cible):
    debut = 0
    fin = len(tableau) - 1
    while debut <= fin:
        milieu = (fin+debut)//2
        if cible == tableau[milieu]:
            return True
        if cible > tableau[milieu]:
            debut = milieu+1
        else:
            fin = milieu-1
    return False


# Tests
assert dichotomie([1, 2, 3, 4], 2)
assert dichotomie([1, 2, 3, 4], 1)
assert dichotomie([1, 2, 3, 4], 4)
assert not dichotomie([1, 2, 3, 4], 0)
assert not dichotomie([1, 2, 3, 4], 5)
assert dichotomie([1], 1)
