# Tests
assert est_bien_parenthesee("([()[]]{()})")
assert not est_bien_parenthesee("{}[(])")
assert not est_bien_parenthesee("[][")
assert not est_bien_parenthesee("[]]")

# Tests supplémentaires
assert est_bien_parenthesee("(" * 10 + ")" * 10)
assert est_bien_parenthesee("([" * 10 + "])" * 10)
assert est_bien_parenthesee("([{" * 10 + "}])" * 10)
assert not est_bien_parenthesee("{" * 10 + "}" * 10 + ")")
assert not est_bien_parenthesee("{" * 10 + ")" * 10)
