# Commentaires

{{ IDE('exo_corr') }}

On suit la démarche décrite dans l'énoncé. Pour chaque caractère (délimiteur) de la chaîne :

* si c'est un délimiteur ouvrant (`if delimiteur in "([{":`), on l'empile,
* sinon :
    * on vérifie que la pile est non-vide (si elle l'est on renvoie `False`),
    * on vérifie que le délimiteur au sommet de la pile correspond à celui lu. Si ce n'est pas le cas, on renvoie `False`,
* en fin de parcours, l'expression est bien parenthésée si la pile est vide : on renvoie simplement le résultat de `pile.est_vide()`.