#--- HDR ---#
class Pile:
    "Classe définissant une structure de pile"

    def __init__(self):
        self.contenu = []

    def est_vide(self):
        "Renvoie le booléen True si la pile est vide, False sinon"
        return self.contenu == []

    def empile(self, v):
        "Place l'élément v au sommet de la pile"
        self.contenu.append(v)

    def depile(self):
        """
        Retire et renvoie l'élément placé au sommet de la pile,
        si la pile n'est pas vide
        """
        if not self.est_vide():
            return self.contenu.pop()
#--- HDR ---#

def est_bien_parenthesee(expression):
    ouvrants = {')': '(',
                ...
                }

    pile = Pile()
    for delimeteur in expression:
        if delimeteur in "([{":
            pile.empile(...)
        else:
            if ...:
                return ...
            elif pile.depile() != ...:
                return ...

    return pile....


# Tests
assert     est_bien_parenthesee('([()[]]{()})')
assert not est_bien_parenthesee('{}[(])')
assert not est_bien_parenthesee('[][')
assert not est_bien_parenthesee('[]]')
