class Entier:
    def __init__(self):
        "Crée un entier égal à zéro"
        self._liste = []

    def est_nul(self):
        "Indique si cet entier est nul ou non"
        return self._liste == []

    def incremente(self):
        "Augmente la valeur de cet entier de un"
        self._liste.append(None)

    def decremente(self):
        "Diminue, si possible, la valeur de cet entier de un"
        if self.est_nul():
            raise ValueError("L'entier est nul et n'a pas de prédécesseur")
        else:
            self._liste.pop()

    def copie(self):
        "Renvoie un entier égal à cet entier"
        resultat = Entier()
        resultat._liste = self._liste.copy()
        return resultat

    def __eq__(self, autre):
        "Teste l'égalité de deux entiers"
        return self._liste == autre._liste

    def __repr__(self):
        "Renvoie la représentation d'un objet de cet entier"
        return f"'Entier' représentant le nombre {len(self._liste)}"

    def __str__(self):
        "Renvoie la chaîne de caractère représentant cet entier"
        return f"'Entier' représentant le nombre {len(self._liste)}"

    def __add__(self, autre):
        """
        Additionne cet entier à un autre entier
        Renvoie un nouvel entier
        """
        resultat = self.copie()
        temp = autre.copie()
        while not temp.est_nul():
            temp.decremente()
            resultat.incremente()
        return resultat

    def __sub__(self, autre):
        """
        Soustrait un autre entier à celui-ci
        Renvoie un nouvel entier
        """
        resultat = self.copie()
        temp = autre.copie()
        while not temp.est_nul():
            temp.decremente()
            if resultat.est_nul():
                return "Opération impossible"
            else:
                resultat.decremente()
        return resultat


# Tests
# Nombres usuels
zero = Entier()

un = Entier()
un.incremente()
un_bis = un.copie()

deux = Entier()
deux.incremente()
deux.incremente()
deux_bis = deux.copie()

trois = Entier()
trois.incremente()
trois.incremente()
trois.incremente()
trois_bis = trois.copie()

cinq = Entier()
cinq.incremente()
cinq.incremente()
cinq.incremente()
cinq.incremente()
cinq.incremente()
cinq_bis = cinq.copie()

# Additions
assert zero + zero == zero

assert un + deux == trois
assert un == un_bis and deux == deux_bis, "Les objets ne doivent pas être modifiés"

# Différences
assert zero - zero == zero

assert deux - un == un
assert un == un_bis and deux == deux_bis, "Les objets ne doivent pas être modifiés"

assert un - deux == "Opération impossible"

# Tests supplémentaires
nombres = [Entier()]
for k in range(1, 20):
    n = nombres[-1].copie()
    n.incremente()
    nombres.append(n)
for i in range(10):
    a = nombres[i]
    b = nombres[i + 1]
    assert a + b == nombres[2 * i + 1], f"Erreur sur une addtion"
    assert b - a == nombres[1], f"Erreur sur une soustraction"
    assert a - b == "Opération impossible", f"Erreur sur une soustraction impossible"
