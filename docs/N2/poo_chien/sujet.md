---
author: Nicolas Revéret
title: Chien en POO
tags:
  - 7-POO
---

# La classe Chien

On souhaite dans cet exercice créer une classe `Chien` ayant deux attributs :

* un âge `age` de type `int`,

* un poids `poids` de type `float`.

Cette classe possède aussi différentes méthodes décrites ci-dessous :

* `age_humain(self)` qui renvoie l'âge du chien en « années d'Homme »  ;

* `age_chien(self)` qui renvoie l'âge du chien en « années de Chien »  ;

* `machouille(self, jouet)` qui renvoie son argument, la chaîne de caractères `jouet`, privé de son dernier caractère ;

* `aboie(self, nb_fois)` qui renvoie la chaîne `'Ouaf' * nb_fois`, où `nb_fois` est un entier passé en argument ;

* `veillit(self)` qui modifie l'attribut `age` en lui ajoutant `1` ;

* `mange(self, ration)` qui modifie l'attribut `poids` en lui ajoutant `0.125 * ration`. L'argument `ration` est de type `float`.

On rappelle que si `toutou` est un objet du type `Chien`, on lui applique la méthode `aboie` en saisissant `toutou.aboie(3)`.

```mermaid
classDiagram
class Chien{
       age : int
       poids : float
       age_humain() int
       age_chien() int
       machouille(jouet : str) str
       aboie(nombre : int) str
       veillit() bool
       mange(ration : float) bool
}
```

???+ note "L'âge du chien"

    L'attribut `age` correspond à **l'âge du chien en « années d'Homme ».**
    
    Les chiens n'ayant pas la même espérance de vie que les humains, on exprime aussi l'âge d'un chien en « années de Chien ». La règle est la suivante :

    * un chien ayant $0$ année d'Homme a $0$ année de Chien,
    * un chien ayant $1$ année d'Homme a $20$ années de Chien,
    * un chien ayant $a \ge 2$ années d'Homme a $20 + 4 \times a$ années de Chien.

    D'autre part, on considère qu'un chien ne peut pas vivre plus de 20 années d'Homme. La méthode `veillit` a donc le comportement suivant :

    * si le chien a strictement moins de $20$ ans (années d'Homme), on ajoute `1` à l'attribut `age` et la méthode renvoie `#!py True`,
    * si le chien a 20 ans (années d'Homme), la méthode renvoie `#!py False`,

???+ note "La méthode `mange`"

    On ajoute les contraintes suivantes concernant la méthode `mange` :

    * on vérifie que la valeur de `ration` est comprise entre 0 (exclu) et un dixième du poids du chien (inclus),
    * dans le cas où la condition précédente est satisfaite, on ajoute `0.125 * ration` au poids du `chien`,
    * la méthode renvoie `True` si l'attribut `poids` est modifié, `False` dans le cas contraire.

!!! example "Exemples"

    ```pycon
    >>> medor = Chien(1, 12.0)
    >>> medor.age_humain()
    1
    >>> medor.age_chien()
    20
    >>> medor.poids
    12.0
    >>> medor.machouille("bâton")
    "bâto"
    >>> medor.aboie(3)
    "OuafOuafOuaf"
    >>> medor.veillit()
    True
    >>> medor.age_chien()
    28
    >>> medor.mange(2.0)
    False
    >>> medor.mange(1.0)
    True
    >>> medor.poids
    12.125
    >>> medor.mange(1.2125)
    True
    ```

Compléter le code de la classe `Chien`.

```python
class Chien:
    def __init__(self, age, poids):
        self.... = age
        ...

    def age_humain(self):
        return ...

    def age_chien(self):
        if self.age == ...:
            return 0
        elif ...:
            return ...
        else:
            ...

    def machouille(self, jouet):
        resultat = ""
        for i in range(...):
            ... += ...[...]
        return ...

    def aboie(self, nombre):
        ...

    def veillit(self):
        if self.age ...:
            self.age = ...
            return ...
        else:
            ...

    def mange(self, ration):
        if ... < ration <= ...:
            self.poids = ...
            return ...
        else:
            return ...
```

{{ IDE('exo') }}