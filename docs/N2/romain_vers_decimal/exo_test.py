VALEUR = {"I": 1, "V": 5, "X": 10, "L": 50, "C": 100, "D": 500, "M": 1000}

# Tests

assert valeur_romains("XVI") == 16
assert valeur_romains("MMXXII") == 2022
assert valeur_romains("CDII") == 402
assert valeur_romains("XLII") == 42

# Autres tests

assert valeur_romains("MCMXCI") == 1991
assert valeur_romains("MCMXCIX") == 1999
assert valeur_romains("MMMCMXCIX") == 3999
assert valeur_romains("XCIV") == 94
assert valeur_romains("MDCLXVI") == 1666
for cle in VALEUR:
    assert valeur_romains(cle) == VALEUR[cle]

