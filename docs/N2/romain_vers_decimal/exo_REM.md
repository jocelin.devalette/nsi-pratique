La version itérative de ce sujet, proposée par Franck Chambon, est bien plus facile que la version originale proposée.  Je me demande si on ne devrait pas transformer cet exercice à l'origine "à trous" en exercice "sans trous".  
Cela donnerait pour le sujet :  
  
```python
VALEUR = {"I": 1, "V": 5, "X": 10, "L": 50, "C": 100, "D": 500, "M": 1000}
def valeur_romains(chaine):
    ...

assert valeur_romains("XVI") == 16
assert valeur_romains("MMXXII") == 2022
assert valeur_romains("CDII") == 402
assert valeur_romains("XLII") == 42
```