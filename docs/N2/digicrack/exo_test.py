# tests

def digicode1(a, b, c, d):
    return not bool ((((a ^ b ^ 6) << ((c & 7) + d)) + ((a + c) ** b + d * 17 + 2022)) % 9973)
assert digicrack(digicode1) == (2, 8, 7, 4), "erreur digicode1"

def digicode2(a, b, c, d):
    return not bool ((((a ^ b ^ 6) << ((c & 7) + d)) + ((a + c) ** b + d * 17 + 1749)) % 9973)
assert digicrack(digicode2) == (2, 9, 8, 6), "erreur digicode2"


# autres tests

def digicode3(a, b, c, d):
    return not bool ((((a ^ b ^ 6) << ((c & 7) + d)) + ((a + c) ** b + d * 17 + 171)) % 9973)
assert digicrack(digicode3) in [(4, 9, 9, 5), (7, 4, 6, 7), (7, 8, 4, 2), (8, 4, 5, 7)], "erreur digicode3"

def digicode4(a, b, c, d):
    return not bool ((((a ^ b ^ 6) << ((c & 7) + d)) + ((a + c) ** b + d * 17 + 1634)) % 9973)
assert digicrack(digicode4) in [(1, 5, 5, 3), (5, 7, 0, 1), (6, 1, 5, 8), (8, 9, 4, 8)], "erreur digicode4"

