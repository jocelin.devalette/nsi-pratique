def digicrack(digicode):
    for n in range(10000):
        a, b, c, d = n // 1000, (n // 100) % 10, (n // 10) % 10, n % 10
        if digicode(a, b, c, d):
            return (a, b, c, d)



